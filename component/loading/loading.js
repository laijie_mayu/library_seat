Component({
	options: {
		// multipleSlots: true		// 在组件定义时的选项中启用多slot支持
	},

	/**
	 * 组件的属性列表
	 */
	properties: {},

	/**
	 * 组件的初始数据
	 */
	data: {
		loadingStatus: 'paused'		//loading状态
	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		/**
		 * 启动loading
		 */
		startLoading() {
			let loadingAnimation = wx.createAnimation({
				duration: 500,
				timingFunction: 'ease-in-out',
			})
			loadingAnimation.opacity(1).step()	//透明度过渡到 1
			this.setData({
				loadingOpacity: loadingAnimation.export(),		//执行动画
				loadingStatus: 'running'	//运行loading状态
			})
		},

		/**
		 * 停止loading
		 */
		stopLoading() {
			let loadingAnimation = wx.createAnimation({
				duration: 500,
				timingFunction: 'ease-in-out',
			})
			loadingAnimation.opacity(0).step()	//透明度过渡到 0
			this.setData({
				loadingOpacity: loadingAnimation.export(),		//执行动画
				loadingStatus: 'paused'		//暂停loading状态
			})
		}
	}
})
