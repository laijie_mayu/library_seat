var now = new Date();

var hours = now.getHours();
var minutes = [now.getMinutes().toString().split('')].map(n => {
  return n.length > 1 ? `${n[0]}${n[1]}` : `0${n[0]}`
});


Component({

  behaviors: [],

  properties: {
    pickerShow: {
      type: Boolean,
      observer: function (val) { //弹出动画
        if (val) {
          let animation = wx.createAnimation({
            duration: 500,
            timingFunction: "ease"
          });
          let animationOpacity = wx.createAnimation({
            duration: 500,
            timingFunction: "ease"
          });
          setTimeout(() => {
            animation.bottom(0).step();
            animationOpacity.opacity(0.7).step();
            this.setData({
              animationOpacity: animationOpacity.export(),
              animationData: animation.export()
            })
          }, 0);
        } else {
          let animation = wx.createAnimation({
            duration: 100,
            timingFunction: "ease"
          });
          let animationOpacity = wx.createAnimation({
            duration: 500,
            timingFunction: "ease"
          });
          animation.bottom(-320).step();
          animationOpacity.opacity(0).step();
          this.setData({
            animationOpacity: animationOpacity.export(),
            animationData: animation.export()
          });
        }
        //处理 10以前的数字
        const minutesData = ['00']
        // 选择分钟  需求不需要
        // for (let i = 0; i < 60; i++) {
        //   if (i < 10) {
        //     i = `0${i}`
        //   }
        //   minutesData.push(i)
        // }
        let nowTime = new Date();

        let hourStart = nowTime.getHours();

        if (hourStart < 8 || hourStart > 22) {
          return
        } else {
          let timeArr = []
          for (; hourStart < 22; hourStart++) {

            timeArr.push(hourStart + 1)

          }
          const arr = (this.data.defaultValue).split('-')
          this.setData({
            showFrist: true, //展示第一个
            startValue: arr[0],
            endValue: arr[1],
            timeDataValue: '',
            isSrue: true,
            value: [hours + ':' + minutes],
            // hoursData: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22],
            hoursData: timeArr,
            minutesData: minutesData
          })
        }


      }
    },
    defaultValue: {
      type: String,
      value: 'default value',
    }
  },


  data: {
    selectTime: '',
    startValue: '', //开始时间
    endValue: '', // 结束时间
    timeDataValue: '', // 不选择开始时间结束时间的时间
    showFrist: true, //展示第一个
    isPicking: false,
    pickerShow: false,
    isStartEnd: 0,
    isSrue: true,
    minutesData: [],
    hoursSelect: '', //选择
    minutesSelect: '',
  }, // 私有数据，可用于模板渲染

  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {
      this._initTimeArray();
    },
    moved: function () {},
    detached: function () {},
  },

  // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
  attached: function () {}, // 此处attached的声明会被lifetimes字段中的声明覆盖
  ready: function () {
    this._init();
  },

  pageLifetimes: {
    // 组件所在页面的生命周期函数
    show: function () {

    },
    hide: function () {},
    resize: function () {},
  },

  methods: {


    // 取消
    _pickerHide: function () {

      //  // 自定义一个事件，并且传值
      let startValue = this.data.startValue
      let endValue = this.data.endValue

      this.triggerEvent('_pickerHide', {
        pickerShow: false
      }, {})
      this.setData({
        pickerShow: false,
        showFrist: true, //展示第一个
        isPicking: false,
        startValue: startValue,
        endValue: endValue
      });
    },
    handlePickStart: function (e) {},
    handlePickEndFirst: function (e) {},
    // 监控滑动 开始的滑动
    changeDateTime: function (e) {
      let val = e.detail.value
      let hoursSelect = this.data.hoursData[val[0]]
      let minutesSelect = this.data.minutesData[val[1]]
      if (this.data.showFrist) {
        this.setData({
          hoursSelect: hoursSelect,
          minutesSelect: minutesSelect,
          startValue: [hoursSelect + ':' + minutesSelect]
        });
      } else {
        this.setData({
          hoursSelect: hoursSelect,
          minutesSelect: minutesSelect,
          endValue: [hoursSelect + ':' + minutesSelect]
        });
      }
    },
    // 获取默认时间为今日时间
    _init: function () {
      if (this.data.isPicking) {
        this.setData({
          startValue: [hours + ':' + '00'],
          showFrist: true,
          isPicking: false,
        });
      } else {
        this.setData({
          endValue: [hours + ':' + '00'],
          showFrist: false,
          isPicking: true,
        });
      }

    },
    // 初始化时间列表
    _initTimeArray: function () {

    },

    // 点击选择开始时间
    _getStartValue: function (e) {
      let that = this
      let startTime = that.data.startValue
      if (startTime === '') {
        that.setData({
          isPicking: true,
        })
        that._init();
      } else {
        that.setData({
          isPicking: true,
          showFrist: true
        })
      }

    },
    // 点击选择结束时间
    _getEndValue: function (e) {
      let that = this
      // let hour = that.data.hoursSelect
      let endTime = that.data.endValue
      if (endTime === '') {
        that.setData({
          isPicking: false,
        })
        that._init();

      } else {
        that.setData({
          isPicking: false,
          showFrist: false
        })
      }
    },
    // 确认
    onConfirm: function () {
      let startValue = this.data.startValue
      let endValue = this.data.endValue
      if (!startValue) {
        wx.showToast({
          icon: "none",
          title: "请选择开始时间"
        });
        return
      }
      if (!endValue) {
        wx.showToast({
          icon: "none",
          title: "请选择结束时间"
        });
        return
      }

      let intStart = parseInt(startValue.toString().split(":")[0]) * 100
      let intEnd = parseInt(endValue.toString().split(":")[0]) * 100
      // let intStart=parseInt(startValue[0].split(":")[0]) * 100 + parseInt(startValue[0].split(":")[1]);
      // let intEnd=parseInt(endValue[0].split(":")[0]) * 100 + parseInt(endValue[0].split(":")[1]);

      if (intStart > intEnd || intStart === intEnd) {
        wx.showToast({
          icon: 'none',
          title: '结束时间不能早于或等于开始时间',
        })
      } else {
        this.triggerEvent('confirmTime', {
          startTime: startValue,
          endTime: endValue

        }, {})
        //提交 隐藏时间选择
        this._pickerHide();

        this.setData({
          showFrist: true, //展示第一个
          isPicking: false,
          pickerShow: false
        })

      }
    }
  }
})