//基础路径
const baseUrl = 'http://agitator.cn.utools.club/';

/**
 * 参数说明
   parmas:{},是一个对象
 */
let countClient = 0;


/**
 * 
   // 客户端获取 基本数据
 */
export const client = (parmas) => {
//加个loading
  countClient++;
  wx.showLoading({
    title: '加载中',
    mask: true
  });
  
 
  if(parmas.method === undefined) {
    parmas.method = 'get';//设置默认值
    parmas.header = {'content-type': 'application/json'};//设置默认值
  }

    if(parmas.method.toLowerCase() === 'post') {
      parmas.header={
        "content-type":"application/x-www-form-urlencoded",
      }
    } else {
    	parmas.header={
	      'content-type': 'application/json'
	    }
    }
  
  return new Promise((resolve,reject) => {
    console.log(parmas)
    wx.request({
      ...parmas,
      url: baseUrl+parmas.url,
      success:res => {
        resolve(res)
      },
      fail:err => {
        reject(err)
      },
      complete:()=> {
      //无论成功失败都关闭loading
        countClient--;
        if(countClient === 0) {
          wx.hideLoading()
        }
      }
    });
  })
};
