// import classList from '../../data/classList'
import {
  client
} from "../../utils/request.js"
var {
  throttle,
  debounce
} = require('../../utils/tools');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    // 时间控件开始
    isPickerRender: false,
    isPickerShow: true,
    //时间选择
    timeSelect: '',
    defautHours: '',
    endHours: '',
    postTiemToString: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取系统时间
    var now = new Date();
    let defautHours = now.getHours() ;
    if (defautHours < 8) {
      defautHours = 8
    }
    let endHours = defautHours + 4
    if (endHours > 22) {
      endHours = 22
    }
    let ss = parseInt(defautHours)
    let ee = parseInt(endHours)
    let postArr = []
    for (let i = ss; i <= ee; i++) {
      postArr.push(i)
    }
    let postTiemToString = postArr.toString()

    this.setData({
      postTiemToString: postTiemToString,
      // list: classList.list.list,
      defautHours: defautHours,
      endHours: endHours,
      timeSelect: defautHours + ':' + '00' + '-' + endHours + ':' + '00'
    })
  },

  onShow: function (e) {
    let postTiemToString = this.data.postTiemToString
    let that = this
    //展示页面发送默认请求

    //得到名字
    wx.getStorage({
      key: 'data',
      success(res) {
        let saetList = res.data.data.seat
        client({
          url: '/seatInfo/getFloorInfoByTimeSlot',
          method: "post",
          data: {
            schoolName: 'HR',
            timeSlot: postTiemToString
          }
        }).then(res => {
          let selectList = []
          let list = res.data
          let objArr = list.map(item => {
            let roomName = saetList[item._id][0].roomName
            let _id = item._id
            let num = item.num
            let objArr = {
              _id,
              roomName,
              num
            }
            return objArr
          })
          selectList.push(objArr)
          that.setData({
            list: selectList[0]
          })
        }).catch(err => {
          console.log(err);
        })
      }
    })


  },

  //时间选择控件
  test: function (e) {
    this.setData({
      isPickerShow: false,
      isPickerRender: false,
    });
  },

  //组件通信
  _pickerHide(e) {
    this.setData({
      isPickerShow: !e.detail.pickerShow,
      isPickerRender: false,
    });
  },
  //得到开始时间和结束时间  重新渲染
  confirmTime(e) {
    let endTime = e.detail.endTime.toString();
    let startTime = e.detail.startTime.toString();

    let postEndTime = endTime.split(":")
    let postStartTime = startTime.split(":")

    let ss = parseInt(postStartTime[0])
    let ee = parseInt(postEndTime[0])
    let postArr = []
    for (let i = ss; i <= ee; i++) {
      postArr.push(i)
    }
    let postTiemToString = postArr.toString()



    let that = this
    that.setData({
      postTiemToString: postTiemToString,
      //选择请求时间
      defautHours: postStartTime[0],
      endHours: postEndTime[0],
      //插槽时间
      timeSelect: startTime + '-' + endTime
    })
  },

  /**
   * 教室点击事件
   * 
   */
  click: function (e) {
    let floor_id = e.currentTarget.dataset.id
    let floor_class = e.currentTarget.dataset.class
    let nowTime = this.data.timeSelect
    wx.navigateTo({
      url: '../select-details/select-detailst?id=' + floor_id,
      success: function (res) {
        // 通过eventChannel向被打开页面传送数据  把楼层id传递
        res.eventChannel.emit('acceptDataFromOpenerPage', {
          class_id: floor_id,
          class_name: floor_class,
          now_time: nowTime
        })
      }
    })
  },
})