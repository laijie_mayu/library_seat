const {
	throttle
} = require("../../utils/tools");
import {
	client
} from "../../utils/request.js"
// pages/index/index.js
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		currentBooking: {},
		timer: '', //定时器任务
		animationData: {},
		animate: false,
		date: {},
		currentOrderData: [{
			seatNo: 303066,
			room: '3F 电子商务书库',
			time: '14:00 - 16:30'
		}],
		flag: '',
		// 时间轴
		timelineDataList: [],
		timelineData: [],
		nodeHeight: [],
		outsideContainerBlurStyle: 'blur(0rpx)',
		isScan: false
	},
	//处理日期
	getweekday: function (date) {
		let weekArray = new Array("日", "一", "二", "三", "四", "五", "六")
		let week = weekArray[date]
		return week
	},
	onLoad: function () {
		let that = this
		// that.init();

		let date = new Date();
		let year = date.getFullYear()
		let month = date.getMonth() + 1
		let day = date.getDate()
		let weekday = date.getDay()
		let week = that.getweekday(weekday)

		that.setData({
			date: {
				year: year,
				month: month,
				day: day,
				weekday: week
			}
		})
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onShow: function (options) {
		let that = this;

		that.init();

	


	},
	/**
	 ** 请求的方法
	 */
	init: function () {
		// 请求 预约信息
		let that = this

		let url = 'http://agitator.cn.utools.club/'

		//选座座位信息
		wx.getStorage({
			key: 'userInfo',
			success(res) {
				let username = res.data.username
				let schoolName = 'HR'
				wx.request({
					url: url + 'seatInfo/getMySeatHistory',
					data: {
						username,
						schoolName
					},
					method: "POST",
					success: function (res) {
						// console.log(res.data);
						let timelineData = [] //定义空数组追加
						let arr = res.data.data;
						let array = arr.slice(-5)
						//处理数据
						let arrObj = array.map(function (item, index, arr) {
							let _id = item._id
							let roomName = item.seat_info[0].roomName
							let id = index + 1
							let room = item.seat_info[0].room
							let date = item.createdTime.substring(5, 10);
							let seatId = item.seatId
							let time = item.timeSlotArr[0] + ":00-" + item.timeSlotArr[item.timeSlotArr.length - 1] + ":00"
							let stat = item.stat


							// let arrObj = {
							// 	id,
							// 	date,
							// 	item: [{
							// 		room,
							// 		seatId,
							// 		_id,
							// 		time,
							// 		roomName,
							// 		stat,
							// 		dotColor: '#EB3941',
							// 		msg:"已失效"
							// 	}],
							// 	dotColor: '#EB3941'
							// }
							// return arrObj
							// let value = wx.getStorageSync('currReserve')
							let  	dotColor = ''
							let msg = ''
							if (stat === 0) {
								msg="未签到"
								dotColor='#00bcd4'
							} else if(stat ===1 ) {
								dotColor='#EB3941'
								msg="已签到"
							}else{
								dotColor='#ccc'
								msg="已失效"
							}

							
							let arrObj = {
								id,
								date,
								item: [{
									room,
									seatId,
									_id,
									time,
									roomName,
									stat,
									dotColor,
									msg
								}],
								dotColor
							}
						  return  arrObj
						});
						// console.log(arrObj);
						timelineData.push(arrObj)
						// let timelineDataReverse =	timelineData.reverse()

						// console.log(timelineDataReverse);
						// 控制记录条数 直接写死了！！
						let listTemp = []
						for (let item in arrObj) {
							if (item >= 6) {
								break
							}
							listTemp.push(arrObj[item])
						}
						let listTempRe = listTemp.reverse()
						that.setData({
							timelineDataList: listTempRe,
							username,
							schoolName
						})
						let nodeHeightTemp = that.data.nodeHeight
						for (let item in listTemp) {
							let selectNode = '#node' + (Number(item) + 1)
							let dividerHeight = wx.createSelectorQuery()
							dividerHeight.select(selectNode).boundingClientRect(rect => {
								nodeHeightTemp.push(rect.height - 50)
								that.setData({
									nodeHeight: nodeHeightTemp
								})
							}).exec();
						}
					},
				})
				// 当前座位信息
				let value = wx.getStorageSync('currReserve')
				if(!value){
					let url = 'http://agitator.cn.utools.club/'
					wx.request({
						url: url + 'seatInfo/currentReserve',
						data: {
							username,
							schoolName
						},
						success: function (res) {
							// console.log(res.data);
							that.setData({
								flag: res.data.flag,
								msg: res.data.msg,
								data: res.data.data,
								_id: res.data._id,
								stat:res.data.stat
							})
						},
					})
				}else{
					wx.getStorage({
						key: 'currReserve',
						success (res) {
							// console.log(res.data);
							let endTime = res.data.timeSlotArr
							let endHours = endTime[endTime.length-1]
							let  myDate=new Date()
							let currHours =myDate.getHours()
							if(endHours >= currHours){
								that.setData({
									data:res.data,
									isScan:true
								})
							}
						}
					})
				}
			}
		})

	},

	/**
	 * 扫码签到
	 * @param {*} showLength 
	 */
	scanSign: function (e) {
		console.log(e.target.dataset);
		let that = this
		let schoolName = 'HR'
		let historyId = e.target.dataset._id
		wx.scanCode({
			success(res) {
				// 缓存起当前预约的状态
				wx.setStorage({
					key:"currReserve",
					data:that.data.data
				})
				client({
					url: 'seatInfo/signInForReserve',
					method: "post",
					data: {
						historyId,
						schoolName 
					}
				}).then(data => {
					console.log(data);
					wx.showToast({
						title: '签到成功',
						icon: 'success',
						duration: 2000
					})
					that.init();
			  that.setData({
					isScan:true
				})
				}).catch(err => {
					console.log(err)
				})
			}
		})
	},
	/**
	 * 签退
	 * @param {*} showLength 
	 */
	scanSignOut: function (e) {
		let that = this
		let schoolName = 'HR'
		let username =that.data.username
		let historyId = e.target.dataset._id
		wx.showModal({
			content: '确认签退吗？',
			success(res) {
				if (res.confirm) {
					client({
						url: 'seatInfo/signOutForReserve',
						method: "post",
						data: {
							historyId,
							schoolName 
						}
					}).then(data => {
						console.log(data)
						wx.removeStorage({
							key: 'currReserve',
						})
						//签退之后 重新获取
						let url = 'http://agitator.cn.utools.club/'
						wx.request({
							url: url + 'seatInfo/currentReserve',
							data: {
								username,
								schoolName
							},
							success: function (res) {
								that.init()
								that.setData({
									flag: res.data.flag,
									msg: res.data.msg,
									data: res.data.data,
									_id: res.data._id,
									data:{
										stat:res.data.stat
									}
								})
							},
						})
					}).catch(err => {
						console.log(err)
					})
				} else if (res.cancel) {
					console.log('用户点击取消')
				}
			}
		})
	},



	toPersonal: throttle(function (e) {
		//跳转到选择页面
		wx.reLaunch({
			url: '../space_new/space_new',
		})
	}, 200),


	/**
	 * 取消当前预约
	 */
	cancelOrder: function (e) {
		let that = this
		let stat = e.currentTarget.dataset.stat
		if (stat === 0) {
			wx.navigateTo({
				url: '../order-details/order-detailst',
				success: function (res) {
					// 通过eventChannel向被打开页面传送数据
					res.eventChannel.emit('acceptDataFromOpenerPage', {
						data: e.currentTarget.dataset
					})
				}
			})
		} else {
			return
		}


	},



	/**
	 * 底部按钮
	 */
	bigButton: throttle(function (e) {
		let that = this
		this.setData({
			outsideContainerBlurStyle: 'blur(60rpx)'
		})
		if (!that.data.animate) {
			that.setData({
				animate: true
			})
			that.animate('.scan', [{
					bottom: "40rpx",
					left: "43.3%",
					ease: "ease-in",
					scale: [0, 0],
					backgroundColor: 'rgb(255, 166, 0,0.1)'
				},
				{
					bottom: "80rpx",
					left: "23.3%",
					ease: "ease-in",
					scale: [1, 1],
					backgroundColor: 'rgb(255, 166, 0)'
				},
			], 500, function () {
				that.clearAnimation('.scan', {
					ease: true,
				}, function () {

				})
			}.bind(that))
			that.animate('.order', [{
					bottom: "40rpx",
					left: "43.4%",
					ease: "ease-in",
					scale: [0, 0],
					backgroundColor: 'rgb(255, 166, 0,0.1)'
				},
				{
					bottom: "200rpx",
					left: "43.4%",
					ease: "ease-in",
					scale: [1, 1],
					backgroundColor: 'rgb(255, 166, 0)'
				},
			], 500, function () {
				that.clearAnimation('.order', {
					ease: true,
				}, function () {})
			}.bind(that))
			that.animate('.count', [{
					bottom: "40rpx",
					left: "43.3%",
					ease: "ease-in",
					scale: [0, 0],
					backgroundColor: 'rgb(255, 166, 0,0.1)'
				},
				{
					bottom: "80rpx",
					left: "63.3%",
					ease: "ease-in",
					scale: [1, 1],
					backgroundColor: 'rgb(255, 166, 0)'
				},
			], 500, function () {
				that.clearAnimation('.count', {
					ease: true,
				}, function () {

				})
			}.bind(that))
			//旋转动画
			that.animate('.bigIcon', [{
					rotate: 90,
					ease: "ease-in"
				},
				{
					rotate: 135,
					ease: "ease-in"
				},
			], 500, function () {
				that.clearAnimation('.bigIcon', {
					ease: true
				}, function () {})
			}.bind(that))
		} else {
			this.setData({
				outsideContainerBlurStyle: 'blur(0rpx)'
			})
			that.setData({
				animate: false
			})
			that.animate('.scan', [{
					bottom: "80rpx",
					left: "23.3%",
					ease: "ease-out",
					scale: [1, 1],
					backgroundColor: 'rgb(255, 166, 0)'
				},
				{
					bottom: "40rpx",
					left: "43.3%",
					ease: "ease-out",
					scale: [0, 0],
					backgroundColor: 'rgb(255, 166, 0,0.1)'
				},
			], 500, function () {
				that.clearAnimation('.scan', {
					ease: true,
				}, function () {})
			}.bind(that))
			that.animate('.order', [{
					bottom: "200rpx",
					left: "43.3%",
					ease: "ease-out",
					scale: [1, 1],
					backgroundColor: 'rgb(255, 166, 0)'
				},
				{
					bottom: "40rpx",
					left: "43.3%",
					ease: "ease-out",
					scale: [0, 0],
					backgroundColor: 'rgb(255, 166, 0,0.1)'
				},
			], 500, function () {
				that.clearAnimation('.order', {
					ease: true,
				}, function () {})
			}.bind(that))
			that.animate('.count', [{
					bottom: "80rpx",
					left: "63.3%",
					ease: "ease-out",
					scale: [1, 1],
					backgroundColor: 'rgb(255, 166, 0)'
				},
				{
					bottom: "40rpx",
					left: "43.3%",
					ease: "ease-out",
					scale: [0, 0],
					backgroundColor: 'rgb(255, 166, 0,0.1)'
				},
			], 500, function () {
				that.clearAnimation('.count', {
					ease: true,
				}, function () {

				})
			}.bind(that))

			//旋转动画
			that.animate('.bigIcon', [{
				rotate: 45,
				ease: "ease-out"
			}, {
				rotate: 0,
				ease: "ease-out"
			}, ], 500, function () {
				that.clearAnimation('.bigIcon', {
					ease: true,
					rotate: true
				}, function () {})
			}.bind(that))
		}

	}, 750),

	/**
	 *  三个子按钮
	 */
	//扫一扫
	scanTouch: throttle(function (e) {
		wx.vibrateShort();
		// 只允许从相机扫码
		wx.scanCode({
			onlyFromCamera: true,
			success(res) {
				console.log(res)
			}
		})
	}, 200),

	//预定座位
	orderTouch: throttle(function (e) {
		wx.vibrateShort();
		//跳转到选择页面
		wx.navigateTo({
			url: '../select_new/select_new',
		})
	}, 200),

	//数据分析
	countTouch: throttle(function (e) {
		let that = this;
		wx.vibrateShort();
	}, 200),

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	},

})