// pages/order-details/order-detailst.js
import {
  client
} from "../../utils/request.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Currinformation: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const eventChannel = this.getOpenerEventChannel()
    let that = this
    // 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
    eventChannel.on('acceptDataFromOpenerPage', function (data) {
      that.setData({
        Currinformation: data.data
      })
    })
  },
  /**
   * 
   * 取消预约
   * @param {*} e 
   */
  cancelOrder: function (e) {
    let that = this
    let historyId = that.data.Currinformation._id
    let seatId = that.data.Currinformation.seatno
    let schoolName = "HR"
    wx.showModal({
      content: '确认取消预约吗？取消积分将不退还',
      success(res) {
        if (res.confirm) {
          client({
            url: 'seatInfo/cancelForServe',
            method: "post",
            data: {
              historyId,
              schoolName
            }
          }).then(data => {
            if (data.data.flag) {
              wx.reLaunch({
                url: '../index/index'
              })
            }
          }).catch(err => {
            console.log(err)
          })
        } else if (res.cancel) {
          return
        }
      }
    })
  }
})