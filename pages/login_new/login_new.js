let app=getApp();     // 取得全局App
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		lastTime: Date.now(),
		movableFrameAnimation: null,
		loginButtonAnimation: null,
		logoAnimation: null,
		putAwayStartPosition: null,
		username: '',
		password: '',
		address:'',
		nextButtonHidden: 'next-button-hidden',
		keyboardHeight: null,
		outsideContainerBlurStyle: 'blur(10rpx)',
	},

	onShow(e) {
		this.animate('.mask', [
			{ ease: 'ease-out'},		
		], 500, function () {		
		}.bind(this))
		setTimeout(()=>{this.setData({
			movableFrameAnimation: 'animated slideInUp ',
		})}, 500)
		
		let loginButtonAnimation = wx.createAnimation({	//主界面登录按钮的动效
			duration: 500,
			timingFunction: 'ease-in-out'
		})
		let logoAnimation = wx.createAnimation({	//主界面logo的动效
			duration: 500,
			timingFunction: 'ease-in-out'
		})
		loginButtonAnimation.opacity(0).step()
		logoAnimation.translateY(-100).step()
		this.setData({
			loginButtonAnimation: loginButtonAnimation.export(),
			logoAnimation: logoAnimation.export()
		})
	},
	/**
	 * 收起login-frame的触摸开始事件
	 */
	putAwayTouchStart(e) {
		this.setData({
			putAwayStartPosition: e.touches[0].pageY	//记录触摸开始的Y坐标
		})
	},

	/**
	 * 收起login-frame的触摸结束事件
	 */
	putAwayTouchEnd(e) {
		let putAwayEndPosition = e.changedTouches[0].pageY	//记录触摸结束的Y坐标
		if (putAwayEndPosition - this.data.putAwayStartPosition >= 50) {	//计算滑动距离
			this.closeLoginFrame()	//收起login-frame
		}
	},

	/**
	 * 显示登录窗体
	 */
	openLoginFrame() {
	
	},

	/**
	 * 关闭登录窗体
	 */
	closeLoginFrame() {
		this.setData({
			movableFrameAnimation: 'animated slideOutDown',
		})
		let loginButtonAnimation = wx.createAnimation({	//主界面登录按钮的动效
			duration: 500,
			timingFunction: 'ease-in-out'
		})
		let logoAnimation = wx.createAnimation({	//主界面logo的动效
			duration: 500,
			timingFunction: 'ease-in-out'
		})
		loginButtonAnimation.opacity(1).step()
		logoAnimation.translateY(0).step()
		this.setData({
			loginButtonAnimation: loginButtonAnimation.export(),
			logoAnimation: logoAnimation.export()
		})
	},

	/**
	 * 用户名输入框输入事件
	 */
	usernameInput(e) {
		this.setData({
			username: e.detail.value	//接收input框的内容
		})
	},

	/**
	 * 密码输入框输入事件
	 */
	passwordInput(e) {
		this.setData({
			password: e.detail.value	//接收input框的内容
		})
		if (this.data.username !== '' && e.detail.value.length >= 1) {
			this.setData({
				nextButtonHidden: ''	//显示next-button
			})
		}
		else {
			this.setData({
				nextButtonHidden: 'next-button-hidden'	//隐藏next-button
			})
		}
	},

	/**
	 * 登录前检查事件
	 */
	loginCheck(e) {
		if (this.data.username == '' || this.data.password == '') {
			wx.showModal({
				title: '',
				content: '用户名和密码不能为空！'
			})
			return
		}else{
			let that = this;
			let username = that.data.username
			let password = that.data.password
			let schoolName =  "HR"
			wx.request({
				url: 'http://agitator.cn.utools.club/users/login',
				method: 'POST',
				data: {
					username,
					password,
					schoolName
				}, 
				header: {
					'content-type': 'application/x-www-form-urlencoded',
					'Accept': 'application/json'
				},
				success: function (res) {
					if(res.data.flag){
						that.setData({
							address:res.data.userInfo.address
						 })
					
						wx.showLoading({
							title:"登录成功",
						})
						setTimeout(function () {
							wx.hideLoading()
						}, 1000)	
		that.getUserProfile() //拿基本信息
	
					}else{
						wx.showLoading({
							title: "登录失败",
						})
						setTimeout(function () {
							wx.hideLoading()
						}, 2000)
						return
					}
				},
				fail:function(res){
						console.log(res.data)
				}
			})
		}
   
	},

		/**
	 * 阅读条款事件
	 */
	checkboxChange(e){
		const values = e.detail.value
	},


/**
	 *  // 新方法获取信息
	 */
 
  getUserProfile(e) {  
	
    let value = wx.getStorageSync('userInfo')
    if (value) {
      //如果我的学号变了 那我的缓存就要重新设置
      let changeUserInfo = wx.getStorageSync('userInfo')
      if (changeUserInfo.username === this.data.username) {
        return
      }
			changeUserInfo.username = this.data.username
			changeUserInfo.address= this.data.address
      app.globalData.userInfo = changeUserInfo
      wx.setStorage({
        key: "userInfo",
        data: changeUserInfo
      })
    } else {
      wx.getUserProfile({
				desc: '完善个人资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
				lang:"zh_CN",
        success: (res) => {
					res.userInfo.username = this.data.username
					res.userInfo.address=this.data.address
					app.globalData.userInfo = res.userInfo
          wx.setStorage({
            key: "userInfo",
            data: res.userInfo
					})
					wx.reLaunch({
						url: '../index/index' 
					})
        },
        fail:()=>{
          wx.redirectTo({
            url: '../login_new/login_new'
          })
        }
      })
    }
  },

	/**
	 * 输入框聚焦事件
	 */
	handleFocus(e) {
		this.setData({
			keyboardHeight: e.detail.height	//获取键盘高度
		})
	},

	/**
	 * 输入框失焦事件
	 */
	handleBlur() {
		this.setData({
			keyboardHeight: 0	//键盘高度归零
		})
	},

		//处理日期
		getweekday: function (date) {
			let weekArray = new Array("日", "一", "二", "三", "四", "五", "六")
			let week = weekArray[date]
			return week
		},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
	
		this.loading = this.selectComponent('#loading')		//选择loading组件
		wx.getStorage({
			key: 'userInfo',
			success (res) {
				wx.redirectTo({
					url: '../index/index'
				})
			}
		})

		let that = this
		// that.init();

		let date = new Date();
		let year = date.getFullYear()
		let month = date.getMonth() + 1
		let day = date.getDate()
		let weekday = date.getDay()
		let week = that.getweekday(weekday)

		that.setData({
			date: {
				year: year,
				month: month,
				day: day,
				weekday: week
			}
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {
		let backgroundAnimation = wx.createAnimation({
			duration: 500,
			timingFunction: 'linear',
		})
		wx.startAccelerometer({	//开始监听加速度传感器
			interval: 'ui',	//回报速率设置为UI(60ms)
			success: res => {
				wx.onAccelerometerChange(res => {
					const now = Date.now()
					if (now - this.data.lastTime < 500) {	//500ms一次动效渲染
						return;
					}
					this.data.lastTime = now;
					let {x, y, z} = res;	//获取加速度x、y、z轴数据
					backgroundAnimation.translateX(-x * 20).translateY(y * 20).scale(1.5).step()	//移动背景图片
					this.setData({
						backgroundAnimation: backgroundAnimation.export()
					})
				})
			}
		})
		this.loading.startLoading()		//启动loading
		setTimeout(() => this.loading.stopLoading(), 500)		//停止loading
	},




})