// app.js
import {client} from "/utils/request.js"
App({
  onLaunch() {
    // 展示本地存储能力
    let that = this
    wx.getSystemInfo({
      success: res => {
        that.globalData.screenHeight = res.screenHeight;
        that.globalData.screenWidth = res.screenWidth;
        that.globalData.statusBarHeight = res.statusBarHeight
      }
    })
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
  // 存放
  onShow(){
    let value = wx.getStorageSync('data')
    if(!value){
      client({
        url:'/seatInfo/getSeatsBySchoolName?schoolName=HR',//这里放入路径就行，服务器地址公共部分已拼接
      }).then(data=> {
        //这里是业务处理逻辑
        wx.setStorage({
          key:"data",
          data:data.data
        })
      }).catch(err=>{
          console.log("err",err)
      })
     
    }
 
  },
  globalData: {
    userInfo: null
  }
 
  
})
